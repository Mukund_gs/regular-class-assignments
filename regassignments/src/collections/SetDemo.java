package collections;

import java.util.HashSet;
import java.util.Set;
public class SetDemo 
{

	public static void main(String[] args)
	{
		Set<String> sports = new HashSet<>();
		sports.add("Football");
		sports.add("Cricket");
		sports.add("Table Tennis");
		sports.add("Badminton");
		System.out.println(sports);
		sports.add("Polo");
		System.out.println(sports);
		int ts= sports.size();
		System.out.println(ts);

	}

}
