package collections;

import java.util.ArrayList;
import java.util.List;

public class Listexample 
{

	public static void main(String[] args) 
	{
		//List Creation
		List<String> list = new ArrayList<>();
		
		list.add("Mango");
		list.add("Apple");
		list.add("Grapes");
		list.add("Banana");
		
		System.out.println(list);
	}

}
