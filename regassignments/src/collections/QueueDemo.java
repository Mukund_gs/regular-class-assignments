package collections;

import java.util.LinkedList;
import java.util.Queue;

public class QueueDemo 
{

	public static void main(String[] args)
	{
		Queue<Integer> numbers=new LinkedList<>();
		
		numbers.offer(109);
		numbers.offer(110);
		numbers.offer(120);
		numbers.offer(125);
		numbers.offer(130);
		System.out.println(numbers);
		int yname=numbers.peek();
		System.out.println("The Head Of the Queue is :"+yname);
		int vname=numbers.poll();
		System.out.println("Removed Number using Poll from the Queue is :"+vname);
		System.out.println(numbers);
	}

}
