package collections;

import java.util.LinkedList;
import java.util.List;

public class LinkedListDemo 
{

	public static void main(String[] args) 
	{
		LinkedList<String> sports= new LinkedList<>();
		
		sports.add("Football");
		sports.add("Cricket");
		sports.add("Table Tennis");
		sports.add("Polo");
		System.out.println(sports);
		sports.add(0,"skiing");
		System.out.println(sports);
		sports.remove(3);
		System.out.println(sports);
		String s = sports.get(2);
		System.out.println(s);
		sports.addFirst("Volleyball");
		System.out.println(sports);
	}

}
