package Interface;

public class B implements A
{
	@Override
	public void method1()
	{
		System.out.println("This is the Interface1 method");
	}
	
	@Override
	public void method2()
	{
		System.out.println(" Method 2 ....This is the Interface method1...");
	}
	
	void thanks()
	{
		System.out.println("This is m own Method.... Thanks Interface A");
	}
	public static void main(String[] args)
	{
		B obj = new B();
		obj.method1();
		obj.method2();
		obj.thanks();
	}
}
