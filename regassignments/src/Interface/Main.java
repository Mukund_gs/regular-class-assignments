package Interface;

interface Continent 
{
	void ContinentName();
}
interface Country
	{
		void CountryName();
	}
interface City
{
	void CityName();
}
public class Main implements Continent, Country, City
{
	@Override
	public void CityName()
	{
		System.out.println("Bangalore");
	}
	@Override
	public void CountryName()
	{
		System.out.println("India");
	}
	@Override
	public void ContinentName()
	{
		System.out.println("Asia");
	}
	public static void main(String[] args)
	{
		Main m = new Main();
		m.CityName();
		m.CountryName();
		m.ContinentName();
	}
}