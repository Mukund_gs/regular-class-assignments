package staticdemo;

public class StaticBlock 
{
	static 
	{
		System.out.println("Checking with main Method1.. which one come first");
	}

	public static void main(String[] args) 
	{
		System.out.println("This is the main Method... Output");
	}

}
