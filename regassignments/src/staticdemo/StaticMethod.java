package staticdemo;

public class StaticMethod 
{
	void play()
	{
		System.out.println("I play tennis...");
	}
	//static Method
	static void sleep()
	{
		System.out.println("I sleep at 10.....");
	}
	public static void main(String[] args) 
	{
		sleep();
		StaticMethod sm = new StaticMethod();
		sm.play();
	}

}
