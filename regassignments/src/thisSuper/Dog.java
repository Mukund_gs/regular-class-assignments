package thisSuper;

class Animal
{
	String color = "White.. Parent class"; 
}

public class Dog extends Animal
{
	String color="Green....";
	void printcolor()
	{
		String color="Black....";
		System.out.println(color);
		System.out.println(this.color);// refers to the current class variable 
		System.out.println(super.color);// prints the parent class 
	}

	public static void main(String[] args) 
	{
		Dog d = new Dog();
		d.printcolor();

	}

}
