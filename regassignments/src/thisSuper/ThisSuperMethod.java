package thisSuper;

class Parent
{
	void play()
	{
		System.out.println("Parent plays Carrom");
	}
}

public class ThisSuperMethod extends Parent
{	
	void play()
	{
		System.out.println("I Play Tennis");
	}
	void all()
	{
		this.play();
		super.play();
	}

	public static void main(String[] args) 
	{
		ThisSuperMethod obj = new ThisSuperMethod();
		obj.all();

	}

}
