package thisSuper;

class MyParent
{
	String Name = "This is the parent Class Instance variable";
}

public class MyClass 
{
	String name="This is an instance Variable";
	int a;
	
	void somemethod()
	{
		String name= "This is a local variable.... because I am inside Method";
		//int b;
		System.out.println(name);
		System.out.println(this.name);
	}
	
	public static void main(String[] args) 
	{
		MyClass mc= new MyClass();
		mc.somemethod();

	}

}
