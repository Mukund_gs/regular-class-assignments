package filehandlingdemo;

import java.io.File;

public class GetFileInformation 
{

	public static void main(String[] args) 
	{
		File obj = new File("C:\\Users\\238783\\Documents\\Java Training Documents\\file2.txt");
		System.out.println(obj.exists());
		
		if(obj.exists())
		{
			System.out.println("Filename : "+obj.getName());
			System.out.println("My File Path Address : "+obj.getAbsolutePath());
			System.out.println("Check Writable or Not : "+obj.canWrite());
			System.out.println("Check Readable or Not : "+obj.canRead());
			System.out.println("File Size in Bytes : "+obj.length());
		}

	}

}
