package garbageCollector;

public class GarbageDemo 
{
	public void finalize()
	{
		System.out.println("unreferenced Object Garbage collected");
	}
	public static void main(String[] args)
	{
	
	GarbageDemo gd = new GarbageDemo();
	gd= null; // Nullify the value 
	GarbageDemo gd1 = new GarbageDemo();// Refer to another object 
	gd = gd1;
	
	// Anonymous 
	new GarbageDemo();
	System.gc();

	}

}
