package Polymorphism;

public class addition 
{
	void sum(int a, int b)
	{
		int c= a+b;
		System.out.println("Total of A and B is : "+c);
	}
	void sum(int a, int b, int c)
	{
		int d = a+b+c;
		System.out.println("The total of a, b and c is : "+d);
	}
	
	void sum(double dd, double ff)
	{
		double rr = dd*ff;
		System.out.println("Product of dd and ff is : "+rr);
	}
	public static void main(String[] args)
	{
		addition obj = new addition();
		obj.sum(55, 66);
		obj.sum(20,30,400);
		obj.sum(55.66, 35343.445);
	}
}
