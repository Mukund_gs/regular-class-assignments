package jdbcdemo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
public class JdbcDems 
{

	public static void main(String[] args) throws SQLException
	{
		Connection obj =DriverManager.getConnection("jdbc:mysql://localhost:3306/firstdatabase","root","root1234");
		
		Statement st = obj.createStatement();
		String abc = "insert into customerdetails values(101,'Wegner',7842737)";
		String xyz = "insert into customerdetails values(102,'Param',787484)";
		String efg = "insert into customerdetails values(103,'Umar',946648)";
		String tfp = "insert into customerdetails values(104,'Xavi',9766632)";
		String oio = "insert into customerdetails values(105,'Kavin',9884749)";
		st.execute(abc);
		st.execute(xyz);
		st.execute(efg);
		st.execute(tfp);
		st.execute(oio);
		obj.close();
		System.out.println("Record Created Succesfully");
	}

}
