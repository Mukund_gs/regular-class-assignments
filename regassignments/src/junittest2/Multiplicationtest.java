package junittest2;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Multiplicationtest 
{
	@Test
	void test()
	{
		Multiplytest m= new Multiplytest();
		assertEquals(30, m.multiply(10, 3));
	}
	
	@BeforeEach
	void testAvg()
	{
		Multiplytest m = new Multiplytest();
		assertEquals(60,m.avg(50,60,70));
	}
	
}
