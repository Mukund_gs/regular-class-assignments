package newFeatures;

interface MyInterface
{
	void method1();
	static void method2()
	{
		System.out.println("THis is the static method");
	}
	default void method3()
	{
		System.out.println("This is the default method");
	}
}
public class A implements MyInterface
{

	@Override
	public void method1() 
	{
		System.out.println("This is override Method");		
	}
	public static void main(String args[])
	{
	A obj = new A();
	obj.method1();
	MyInterface.method2();
	obj.method3();
	}

}
