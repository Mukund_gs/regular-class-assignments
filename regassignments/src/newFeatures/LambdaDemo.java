package newFeatures;

interface Animal
{
	void Method1print();
}
public class LambdaDemo implements Animal
{
	@Override
	public void Method1print() 
	{
		System.out.println("Lion is an animal in the jungle");
		
	}
	public static void main(String[] args) 
	{
		LambdaDemo d= new LambdaDemo();
		d.Method1print();
	}

}
