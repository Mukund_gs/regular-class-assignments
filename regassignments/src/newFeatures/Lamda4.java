package newFeatures;

interface Bus
{
	void method1();
	
	
	
}
public class Lamda4 implements Bus
{
	static void method2()
	{
		System.out.println("This is thw first method");
	}
	@Override
	public void method1()
	{
		System.out.println("THis is the overriden method");
	}

	public static void main(String[] args)
	{
		Lamda4 obj= new Lamda4();
		obj.method1();
		Lamda4.method2();

	}

}
