package newFeatures;
interface Bus
{
	void method1print(String start, String stop);
}

public class LambdaDemo2 
{
	public static void main(String[] args) 
	{
		Bus ref=(start,stop)->System.out.println("Travelling Starts from" +start+ "to" +stop);
		ref.method1print("Bangalore","Hyderarbad");
	}
}
