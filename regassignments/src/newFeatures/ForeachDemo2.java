package newFeatures;

import java.util.ArrayList;
import java.util.List;

public class ForeachDemo2
{
	public static void main(String[] args) 
	{
		List<String> demo= new ArrayList<>();
		demo.add("Apple");
		demo.add("Banana");
		demo.add("Grape");
		demo.add("Fruits");
		
		demo.forEach(name->System.out.println(demo));
	}
}
