package newFeatures;

interface An
{
	void getName(String name);
}
public class Methodref 
{
	public static void main(String[] args) 
	{
		An ref=(String s)->System.out.println(s);
		ref.getName("Getting Name Using Lamda");
		
	}
}
