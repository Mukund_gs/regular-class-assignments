package newFeatures;


interface Cab2
{
	public double method1(String start, String Destination, int km , double rupees);
}

public class Lamda3 
{
 public static void main(String args[])
 {
	 Cab2 abc= (start,Destination,km,rupees)->{
		 System.out.println("Cab Booked from "+start+" to "+Destination+"\nTotal cost is ");
		 return (km*rupees);
	 };
	 System.out.println(abc.method1("Delhi","Mumbai", 1500, 200));
 }
}
